// While Loop
// let count = 1;
// while(count < 11){
// 	console.log('count is ' + count);
// 	count++;
// }

// let num = 1
// while(num <= 10){
// console.log(num)
// num+=2;
// }

// Practice 1
// let num = -10;
// while(num <= 19){
// 	console.log(num)
// 	num++
// }

// Practice 2
// let num = 10;
// while(num <= 40){
// 	if(num%2 === 0){
// 		console.log(num);
// 	}
// 	num++;
// }


// Practice 3
// let num = 300;
// while(num <= 333){
// 	if(num%2 === 1){
// 		console.log(num);
// 	}
// 	num++
// }